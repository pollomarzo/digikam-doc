# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-02 00:35+0000\n"
"PO-Revision-Date: 2023-02-08 08:31+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../slideshow_tools/opengl_viewer.rst:1
msgid "Using digiKam OpenGL Viewer"
msgstr "Користування засобом перегляду OpenGL digiKam"

#: ../../slideshow_tools/opengl_viewer.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, slide, opengl"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, навчання, простий, слайд, opengl"

#: ../../slideshow_tools/opengl_viewer.rst:14
msgid "OpenGL Viewer"
msgstr "Переглядач із OpenGL"

#: ../../slideshow_tools/opengl_viewer.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../slideshow_tools/opengl_viewer.rst:18
msgid ""
"This tool preview a series of items using OpenGL hardware to speed-up "
"rendering. There is no configuration dialog. Calling this tool from :"
"menuselection:`View --> OpenGL Image Viewer` will show items in full-screen "
"mode."
msgstr ""
"За допомогою цього інструмента можна переглядати записи за допомогою "
"апаратного прискорення OpenGL для пришвидшення обробки. Діалогового вікна "
"налаштувань не передбачено. Виклик цього інструмента за допомогою пункту "
"меню :menuselection:`Перегляд --> Переглядач зображень з OpenGL` призведе до "
"показу зображень у повноекранному режимі."

#: ../../slideshow_tools/opengl_viewer.rst:22
msgid ""
"This tool does not include an OSD (On Screen Display). Navigating between "
"items is done with keyboard and mouse."
msgstr ""
"У цьому інструменті не передбачено екранної накладки (OSD або On Screen "
"Display). Навігація між записами здійснюється за допомогою клавіатури та "
"миші."

#: ../../slideshow_tools/opengl_viewer.rst:29
msgid "Screencast of the OpenGL Viewer"
msgstr "Відео роботи переглядача із OpenGL"

#: ../../slideshow_tools/opengl_viewer.rst:31
msgid ""
"The usage from Keyboard and mouse to quickly navigate between items is "
"listen below:"
msgstr ""
"Дані щодо використання клавіатури і миші для швидкої навігації між записами "
"наведено нижче:"

#: ../../slideshow_tools/opengl_viewer.rst:33
msgid "Item Access"
msgstr "Доступ до записів"

#: ../../slideshow_tools/opengl_viewer.rst:36
msgid ":kbd:`Up` key :kbd:`PgUp` key :kbd:`Left` key Mouse wheel up"
msgstr ""
"Клавіша :kbd:`↑`, клавіша :kbd:`PgUp`, клавіша :kbd:`←`, гортання коліщатка "
"миші вгору"

#: ../../slideshow_tools/opengl_viewer.rst:39
msgid "Previous Item:"
msgstr "Попередній запис:"

#: ../../slideshow_tools/opengl_viewer.rst:42
msgid ":kbd:`Down` key :kbd:`PgDown` key :kbd:`Right` key Mouse wheel down"
msgstr ""
"Клавіша :kbd:`↓`, клавіша :kbd:`PgDown`, клавіша :kbd:`→`, гортання "
"коліщатка миші вниз"

#: ../../slideshow_tools/opengl_viewer.rst:45
msgid "Next Item:"
msgstr "Наступний запис:"

#: ../../slideshow_tools/opengl_viewer.rst:48
msgid "Quit:"
msgstr "Вийти:"

#: ../../slideshow_tools/opengl_viewer.rst:48
msgid ":kbd:`Esc` key"
msgstr "Клавіша :kbd:`Esc`"

#: ../../slideshow_tools/opengl_viewer.rst:50
msgid "Item Display"
msgstr "Показ записів"

#: ../../slideshow_tools/opengl_viewer.rst:53
msgid "Toggle fullscreen to normal:"
msgstr "Перемикання з повноекранного режиму на звичайний:"

#: ../../slideshow_tools/opengl_viewer.rst:53
msgid ":kbd:`f` key"
msgstr "Клавіша :kbd:`F`"

#: ../../slideshow_tools/opengl_viewer.rst:56
msgid "Toggle scroll-wheel action:"
msgstr "Перемикання дії коліщатка гортання:"

#: ../../slideshow_tools/opengl_viewer.rst:56
msgid ":kbd:`c` key (either zoom or change image)"
msgstr "Клавіша :kbd:`C` (масштабування або зміна зображення)"

#: ../../slideshow_tools/opengl_viewer.rst:59
msgid "Rotation:"
msgstr "Обертання:"

#: ../../slideshow_tools/opengl_viewer.rst:59
msgid ":kbd:`r` key"
msgstr "Клавіша :kbd:`R`"

#: ../../slideshow_tools/opengl_viewer.rst:62
msgid "Reset view:"
msgstr "Скинути перегляд:"

#: ../../slideshow_tools/opengl_viewer.rst:62
msgid "double click"
msgstr "подвійний клік"

#: ../../slideshow_tools/opengl_viewer.rst:65
msgid "Original size:"
msgstr "Початковий розмір:"

#: ../../slideshow_tools/opengl_viewer.rst:65
msgid ":kbd:`o` key"
msgstr "Клавіша :kbd:`O`"

#: ../../slideshow_tools/opengl_viewer.rst:68
msgid ""
"Move mouse in up-down-direction while pressing the right mouse button :kbd:"
"`c` key and use the scroll-wheel :kbd:`+` and :kbd:`-` keys :kbd:`ctrl` + "
"scrollwheel"
msgstr ""
"Пересування вказівника у напрямку вгору-вниз із одночасним утриманням "
"натиснутою правої кнопки миші, клавіша :kbd:`C` і використання коліщатка "
"гортання, клавіші :kbd:`+` і :kbd:`-`, :kbd:`Ctrl + коліщатко гортання`"

#: ../../slideshow_tools/opengl_viewer.rst:71
msgid "Zooming:"
msgstr "Масштабування:"

#: ../../slideshow_tools/opengl_viewer.rst:74
msgid "Panning:"
msgstr "Панорамування:"

#: ../../slideshow_tools/opengl_viewer.rst:74
msgid "Move mouse while pressing the left button"
msgstr ""
"Пересування вказівника миші під час утримування натиснутою лівої кнопки миші"

#: ../../slideshow_tools/opengl_viewer.rst:76
msgid "Others"
msgstr "Інші"

#: ../../slideshow_tools/opengl_viewer.rst:78
msgid "Show help dialog:"
msgstr "Показ вікна довідки:"

#: ../../slideshow_tools/opengl_viewer.rst:79
msgid ":kbd:`F1` key"
msgstr "Клавіша :kbd:`F1`"

#~ msgid "f key"
#~ msgstr "Клавіша F"

#~ msgid "r key"
#~ msgstr "Клавіша R"

#~ msgid "o key"
#~ msgstr "Клавіша O"
